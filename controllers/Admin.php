<?php
use sngrl\PhpFirebaseCloudMessaging\Client;
use sngrl\PhpFirebaseCloudMessaging\Message;
use sngrl\PhpFirebaseCloudMessaging\Recipient\Device;
use sngrl\PhpFirebaseCloudMessaging\Notification;
class Admin Extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
        if (null == ($this->session->userdata('is_logged_in'))) {
			$this->load->helper('url');
			$this->session->set_userdata('redirect_back', current_url());
            redirect('/auth/login/');
        }
        added by work1-1
        if ($this->session->userdata('admin_role') != 1) {
            redirect('/auth/login/');
        }
        added by work2
        $this->load->model('Appmodel', 'app_model');
        $this->load->model('Crud', 'crud');
        $this->load->helper('url');

    }
    implemented by work1
    
    function sales_report()
    {
        $where['admin_role'] = 2;
        $data['employees'] = $this->crud->get_all_with_where('admin','admin_name','ASC',$where);
        set_page('sales_report',$data);
    }
    
    function sales_report()
    {
        $where['admin_role'] = 2;
        $data['employees'] = $this->crud->get_all_with_where('admin','admin_name','ASC',$where);
        set_page('sales_report',$data);
    }
line 1 by work1-1
added by work2-1
work2-1
    function delete($id)
    {
        $table = $_POST['table_name'];
        $id_name = $_POST['id_name'];
        $this->crud->delete($table, array($id_name => $id));
        $this->session->set_flashdata('success',true);
        $this->session->set_flashdata('message','Deleted Successfully');
    }

    function delete($id)
    {
        $table = $_POST['table_name'];
        $id_name = $_POST['id_name'];
        $this->crud->delete($table, array($id_name => $id));
        $this->session->set_flashdata('success',true);
        $this->session->set_flashdata('message','Deleted Successfully');
    }
    
    function sales_report()
    {
        added by work2
        $where['admin_role'] = 2;
        $data['employees'] = $this->crud->get_all_with_where('admin','admin_name','ASC',$where);
        set_page('sales_report',$data);
    }
    added by master

	public function get_sales_report()
    {
		$where = array();
		$cust_where = array();
        $employee_id = $this->input->post('admin_id');
        $today = date('Y-m-d');
        
        $config['select'] = 'admin.admin_name, planning.planning_id, planning.employee_admin_id, planning.customer_admin_id, planning.planning_date, planning.create_date';
        $config['table'] = 'planning';
        $config['table'] = 'planning';
        $config['wheres'] = array();
        $config['wheres'][] = array('column_name'=>'planning.employee_admin_id','column_value'=> $employee_id );
        $config['joins'][] = array('join_table'=>'admin','join_by'=>'planning.customer_admin_id = admin.admin_id','join_type'=>'left');
        //$config['group_by'] = 'planning.planning_id , admin.admin_name';
        $config['like'] = array('planning.planning_date' => $today);
        $config['column_search'] = array('admin.admin_name', 'planning.planning_date');
        
        $config['order'] = array('planning.planning_id' => 'desc');
        
        $this->load->library('datatables', $config, 'datatable');
        $result = $this->datatable->get_datatables();
        //echo '<pre>';print_r($result);exit;
        
        $data_result = array();
        if($result > 0){
			foreach($result as $row)
			{
				$row_result = array();
                $cust_where['admin_id'] = $row->customer_admin_id;
                $customer_res = $this->crud->get_all_with_where('admin','','',$cust_where);
                $admin_name = "Not Defined";
                $where_emp_id['employee_admin_id'] = $employee_id;
                $where_emp_id['customer_admin_id'] = $customer_res[0]->admin_id;
                $date_like['create_date'] = $today;
                $order_total = $this->crud->get_today_num_order($where_emp_id,$date_like);
                if(!empty($customer_res[0]->admin_name))
                {
                    $admin_name = $customer_res[0]->admin_name;
                }
                $date = $row->planning_date;
                $date = substr($date, 8, 2).'-'.substr($date, 5, 2).'-'.substr($date, 0, 4).' '.substr($date, 11);
                $row_result[] = "<b>Date: </b> ".$date."<br />
                                <b>Customer Name: </b> ".$admin_name."<br />
                                <b>Order: </b>".$order_total->num_rows()."<br />";
				$data_result[] = $row_result;
			}
		}
		
        $output = array(
            "draw" => $_POST['draw'],
            "recordsTotal" => $this->datatable->count_all(),
            "recordsFiltered" => $this->datatable->count_filtered(),
            "data" => $data_result,
        );
        //output to json format
        echo json_encode($output);
        exit;
	}
    
    function customer_complains()
    {
        $where = array();
        $data['records'] = $this->crud->admin_complain_list($where);
        set_page('customer_complains',$data);
    }
    
    function customer_complains()
    {
        $where = array();
        $data['records'] = $this->crud->admin_complain_list($where);
        set_page('customer_complains',$data);
    }
    
    function reply_complain($complain_id)
    {
        $where['complain_id'] = $complain_id;
        $data['complain_id'] = $complain_id;
        $complain = $this->crud->get_complain_detail($where);
        $data['product_name'] = $complain[0]->category_name.' '.$complain[0]->item_name.' '.$complain[0]->company_name.' '.$complain[0]->variant_name;
        $data['create_date'] = $complain[0]->create_date;
        $data['product_image'] = $complain[0]->product_image;
        $data['admin_name'] = $complain[0]->admin_name;
        $data['issue'] = $complain[0]->remark;
        $data['results'] = $this->crud->get_complain_reply($where);
        set_page('reply_complain',$data);
    }

    function submit_complain_reply()
    {
        $data = $this->input->post();
        $data['create_date'] = date('Y-m-d H:i:s');
        $data['sender_id'] = $this->session->userdata('admin_id');
        $result = $this->crud->insert('complain_reply',$data);
        if($result)
        {
            $this->session->set_flashdata('success',true);
            $this->session->set_flashdata('message','Replied Successfully.');
            $return['success'] = "true";
            print json_encode($return);
            exit;
        }
        else
        {
            $this->session->set_flashdata('success',false);
            $this->session->set_flashdata('message','! Error : Not Replied.');
            $return['success'] = "false";
            print json_encode($return);
            exit;
        }
    }

    function add_category()
    {
        $result_data = $this->app_model->get_all_records('category', 'category_id', 'ASC');
        set_page('add_category',array('results' => $result_data));
    }

    function submit_category()
    {
        $post_data = $this->input->post();
        $post_data['create_date'] = date('Y-m-d H:i:s');
        $result = $this->app_model->insert('category',$post_data);
        if ($result) {
            $post_data['category_id'] = $result;
            echo json_encode($post_data);
        }
    }

    function add_item()
    {
        $result_data = $this->app_model->get_all_records('item', 'item_id', 'ASC');
        set_page('add_item',array('results' => $result_data));
    }
    function submit_item()
    {
        $post_data = $this->input->post();
        $post_data['create_date'] = date('Y-m-d H:i:s');
        $result = $this->app_model->insert('item',$post_data);
        if ($result) {
            $post_data['item_id'] = $result;
            echo json_encode($post_data);
        }
    }

    function add_company()
    {
        $result_data = $this->app_model->get_all_records('company', 'company_id', 'ASC');
        set_page('add_company',array('results' => $result_data));
    }

    function submit_company()
    {
        $post_data = $this->input->post();
        $post_data['create_date'] = date('Y-m-d H:i:s');
        $result = $this->app_model->insert('company',$post_data);
        if ($result) {
            $post_data['company_id'] = $result;
            echo json_encode($post_data);
        }
    }

    function add_variant()
    {
        $result_data = $this->app_model->get_all_records('variant', 'variant_id', 'ASC');
        set_page('add_varient',array('results' => $result_data));
    }

    function submit_variant()
    {
        $post_data = $this->input->post();
        $post_data['create_date'] = date('Y-m-d H:i:s');
        $result = $this->app_model->insert('variant',$post_data);
        if ($result) {
            $post_data['variant_id'] = $result;
            echo json_encode($post_data);
        }
    }

    function add_product()
    {
        $data = array(
            'categorys' => $this->crud->get_select_data('category'),
            'items' => $this->crud->get_select_data('item'),
            'companys' => $this->crud->get_select_data('company'),
            'variants' => $this->crud->get_select_data('variant')
        );
        set_page('add_product',$data);
    }

    function submit_product()
    {
        $post_data = $this->input->post();
        $post_data['create_date'] = date('Y-m-d H:i:s');
        $result = $this->app_model->insert('product',$post_data);
        if ($result) {
            $post_data['product_id'] = $result;
            echo json_encode($post_data);
        }
    }
    public function action_approve(){
        $action_txt = "approve";
        $customerId = $_REQUEST['id'];

        if(!empty($customerId)){

            $this->app_model->update_status($customerId,$action_txt);

            $this->session->set_flashdata('success',true);
            $this->session->set_flashdata('message','Successfully Approve.');
            $return['success'] = "true";
            print json_encode($return);
            exit;

        }else{
            $this->session->set_flashdata('success',false);
            $this->session->set_flashdata('message','Rejected.');
            $return['success'] = "false";
            print json_encode($return);
            exit;
        }
    }

    public function action_reject(){
        $action_txt = "reject";
        $customerId = $_REQUEST['id'];

        if(!empty($customerId)){

            $this->app_model->update_status($customerId,$action_txt);

            $this->session->set_flashdata('success',true);
            $this->session->set_flashdata('message','Successfully Rejected.');
            $return['success'] = "true";
            print json_encode($return);
            exit;

        }else{
            $this->session->set_flashdata('success',false);
            $this->session->set_flashdata('message','Rejected.');
            $return['success'] = "false";
            print json_encode($return);
            exit;
        }
    }

    function add_product_offer()
    {
        $data['products'] = $this->crud->product_list();
        set_page('add_product_offer',$data);
    }

    function offer_list()
    {
        $data['records'] = $this->crud->get_product_offer();
        set_page('offer_list',$data);
    }

    function add_offer()
    {
        $data = $this->input->post();
        $offer_image = '';
        if(!empty($_FILES['offer_image']['name'])){
			$config['upload_path'] = 'assets/uploads/offer_image/';
			$config['allowed_types'] = 'jpg|jpeg|png|gif';
			$config['file_name'] = $_FILES['offer_image']['name'];
			
			//Load upload library and initialize configuration
			$this->load->library('upload',$config);
			$this->upload->initialize($config);
			
			if($this->upload->do_upload('offer_image')){
				$uploadData = $this->upload->data();
				$offer_image = $uploadData['file_name'];
			}else{
				$offer_image = '';
				$this->session->set_flashdata('success',false);
				$this->session->set_flashdata('message','!  : Offer Image File Not Uploded.');
				exit;
			}
		}
        $data['offer_image'] = $offer_image;
        $data['create_date'] = date('Y-m-d H:i:s');
        $result = $this->crud->insert('offer',$data);
        if($result)
        {
            $this->session->set_flashdata('success',true);
            $this->session->set_flashdata('message','Offer Added Successfully.');
            exit;
        }
        else
        {
            $this->session->set_flashdata('success',false);
            $this->session->set_flashdata('message','! Error : Offer not added.');
            exit;
        }
    }

    public function confirm_customer()
    {
        $data['records'] = $this->app_model->requested_user();
        set_page('confirm_customer',$data);
    }

    function upload_outstanding()
    {
        set_page('upload_outstanding');
    }

    function admin_knowledge_center()
    {
        if(isset($_REQUEST['product_id'])){
            $product_id = $_REQUEST['product_id'];
            $prod_details = $this->crud->get_data_row_by_id('product','product_id',$product_id);
            echo $prod_details->product_description; exit;
        }
        $data = array(
            'products' => $this->crud->product_list()
        );
        set_page('admin_knowledge_center',$data);
    }
    
        
    function product_desc_edit()
    {
        if($this->input->post()){
            $post_data = $this->input->post();
            $product_details = $post_data['product_details'];
            $product_id = $post_data['txt_product_id'];
            $data = array();
            $data['product_description'] = $product_details;
            //$data['date'] = $date;
            $result = $this->crud->update("product", $data,array('product_id'=>$product_id));
            if($result)
            {
                $this->session->set_flashdata('success',true);
                $this->session->set_flashdata('message','Details Updated Successfully.');
                $return['success'] = "true";
                print json_encode($data);
                exit;
            }
            else
            {
                $this->session->set_flashdata('success',false);
                $this->session->set_flashdata('message','! Error : Details not updated.');
                $return['success'] = "false";
                print json_encode($return);
                exit;
            }
        }
    }
    function get_allcat()
    {
        $result = $this->crud->get_all_cat('category','category_id','category_name');
        $category_arr = array();
        foreach($result as $cat_row){
            $category_arr[] = $cat_row;
        }
        echo json_encode($category_arr);
        return $category_arr;
    }
    function get_allitem()
    {
        $result = $this->crud->get_all_cat('item','item_id','item_name');
        $category_arr = array();
        foreach($result as $cat_row){
            $category_arr[] = $cat_row;
        }
        echo json_encode($category_arr);
        return $category_arr;
    }
    function get_allcompany()
    {
        $result = $this->crud->get_all_cat('company','company_id','company_name');
        $category_arr = array();
        foreach($result as $cat_row){
            $category_arr[] = $cat_row;
        }
        echo json_encode($category_arr);
        return $category_arr;
    }
    function get_allvariant()
    {
        $result = $this->crud->get_all_cat('variant','variant_id','variant_name');
        $category_arr = array();
        foreach($result as $cat_row){
            $category_arr[] = $cat_row;
        }
        echo json_encode($category_arr);
        return $category_arr;
    }
    
    function product_list(){
        $data['records'] = $this->crud->product_list();
        set_page('product_list',$data);
    }
    
    function add_new_product(){
        $data = $_REQUEST;
        $category = $data['category'];
        $item = $data['item'];
        $company = $data['company'];
        $variant = $data['variant'];
        $category_id = 0;
        if(!empty($category)){
            if(is_numeric($category)){
                $category_id = $category;
            } else {
                $category = trim($category);
                $res = $this->crud->get_result_where('category','category_name',$category);
                if(empty($res)){
                    $category_id = $this->crud->insert('category',array("category_name" => $category));
                } else {
                    $category_id = $res[0]->category_id;
                }
            }
        }
        $item_id = 0;
        if(!empty($item)){
            if(is_numeric($item)){
                $item_id = $item;
            } else {
                $item = trim($item);
                $res = $this->crud->get_result_where('item','item_name',$item);
                if(empty($res)){
                    $item_id = $this->crud->insert('item',array("item_name" => $item));
                } else {
                    $item_id = $res[0]->item_id;
                }
            }
        }
        $company_id = 0;
        if(!empty($company)){
            if(is_numeric($company)){
                $company_id = $company;
            } else {
                $company = trim($company);
                $res = $this->crud->get_result_where('company','company_name',$company);
                if(empty($res)){
                    $company_id = $this->crud->insert('company',array("company_name" => $company));
                } else {
                    $company_id = $res[0]->company_id;
                }
            }
        }
        $variant_id = 0;
        if(!empty($variant)){
            if(is_numeric($variant)){
                $variant_id = $variant;
            } else {
                $variant = trim($variant);
                $res = $this->crud->get_result_where('variant','variant_name',$variant);
                if(empty($res)){
                    $variant_id = $this->crud->insert('variant',array("variant_name" => $variant));
                } else {
                    $variant_id = $res[0]->variant_id;
                }
            }
        }
        $where_array = array("category_id" => $category_id,"item_id" => $item_id,"company_id" => $company_id,"variant_id" => $variant_id);
        $row = $this->crud->get_duplicate('product',$where_array);
        if($row < '0'){
            $data = array(
            "category_id" => $category_id,
            "item_id" => $item_id,
            "company_id" => $company_id,
            "variant_id" => $variant_id,
            "create_date" => date('Y-m-d H:i:s')
            );
            $insert = $this->crud->insert('product', $data);
            if($insert){
                $this->session->set_flashdata('success',true);
                $this->session->set_flashdata('message','Product Added Successfully.');
                $return['success'] = "true";
                print json_encode($return);
                //redirect(base_url('admin/add_product_offer'), 'refresh');
                exit;
                
            }else{
                
                $this->session->set_flashdata('success',false);
                $this->session->set_flashdata('message','! Error : Some error occu..');
                $return['success'] = "false";
                //$return['error'] = "Error";
                print json_encode($return);
                exit;
            }    
        }else{
            $this->session->set_flashdata('success',false);
            $this->session->set_flashdata('message','! Error : Product Already Exist.');
            $return['success'] = "false";
            //$return['error'] = "Exist";
            print json_encode($return);
            exit;
        }
    }

    function add_employee()
    {
        set_page('add_employee');
    }

    function save_employee()
    {
        $data = $this->input->post();
        $where['admin_email'] = $data['admin_email'];
        $check_email = $this->crud->get_duplicate_no('admin',$where);
        if($check_email > 0)
        {
            $this->session->set_flashdata('success',false);
            $this->session->set_flashdata('message','! Error : Email already Exist.');
            $return['success'] = "false";
            print json_encode($return);
            exit;
        }
        $data['admin_role'] = '2';
        $data['admin_status'] = 'approve';
        $data['admin_password'] = md5($data['admin_password']);
        $dob = $this->input->post('dob');
        $data['dob'] = substr($dob, 6, 4).'-'.substr($dob, 3, 2).'-'.substr($dob, 0, 2);
        $data['create_date'] = date('Y-m-d H:i:s');
        $result = $this->crud->insert('admin',$data);
        if($result)
        {
            $this->session->set_flashdata('success',true);
            $this->session->set_flashdata('message','Employee Added Successfully.');
            $return['success'] = "true";
            print json_encode($return);
            exit;
        }
        else
        {
            $this->session->set_flashdata('success',false);
            $this->session->set_flashdata('message','! Error : Employee not added.');
            $return['success'] = "false";
            print json_encode($return);
            exit;
        }
    }

	function update_employee()
	{
		$data = $this->input->post();
		//print_r($data);
        //$update['admin_email'] = $data['admin_email'];
        $update['admin_name'] = $data['admin_name'];
        $update['address'] = $data['address'];
        $update['admin_city'] = $data['admin_city'];
               
        $dob = $this->input->post('dob');
        $update['dob'] = substr($dob, 6, 4).'-'.substr($dob, 3, 2).'-'.substr($dob, 0, 2);

        $where['admin_id']=$data['admin_id'];
        
        $result = $this->crud->update('admin',$update,$where);
       
        
        if($result)
        {
            $this->session->set_flashdata('success',true);
            $this->session->set_flashdata('message','Employee Updated Successfully.');
            $return['success'] = "true";
            print json_encode($return);
            exit;
        }
        else
        {
            $this->session->set_flashdata('success',false);
            $this->session->set_flashdata('message','! Error : Employee not updated.');
            $return['success'] = "false";
            print json_encode($return);
            exit;
        }
	}

    function employee_list()
    {
        $where['admin_role'] = '2';
        $data['records'] = $this->crud->get_all_with_where('admin','admin_id','desc',$where);
        set_page('employee_list',$data);
    }

	function employee_edit($id)
    {
		$employee_id = $this->input->get('employee_id');
		//print_r($id);
		//exit;
        //$where['admin_role'] = '2';
        
        $data['records'] = $this->crud->get_data_row_by_id('admin','admin_id',$id);
      //  print_r($data);
		$data['dob'] = substr($data['records']->dob, 8, 2).'/'.substr($data['records']->dob, 5, 2).'/'.substr($data['records']->dob, 0, 4);
        set_page('edit_employee',$data);
    }
    
    function customer_list()
    {
        set_page('customer_list');
    }

    function get_customer()
    {
        $where['admin_role'] = '3';
        $where['admin_status'] = $this->input->post('admin_status');
        //$data = $this->crud->get_customer($where);
        
        $config['select'] = 'admin_id, admin_name, admin_email, organization, address, admin_city, dob';
        $config['table'] = 'admin';
        $config['wheres'] = array();
        $config['wheres'][] = array('column_name' => 'admin_role', 'column_value' => '3');
        $config['wheres'][] = array('column_name'=>'admin_status','column_value'=> $this->input->post('admin_status') );
        $config['column_search'] = array('admin_name','admin_city');
        $config['order'] = array('admin_id' => 'desc');
        
        
        $this->load->library('datatables', $config, 'datatable');
        $list = $this->datatable->get_datatables();
        
        $data_result = array();
        
            foreach($list as $row)
            {
                $row_result = array();
                $date = $row->dob;
                $date = substr($date, 8, 2).'-'.substr($date, 5, 2).'-'.substr($date, 0, 4).' '.substr($date, 11);
                $row_result[] = "<b>Customer Name: </b> ".$row->admin_name."<br />
                                <b>Organization: </b> ".$row->organization."<br />
                                <b>Address: </b> ".$row->address."<br />
                                <b>City: </b> ".$row->admin_city."<br />
                                <b>Date: </b> ".$date."<br />
                                <b>Email: </b> ".$row->admin_email;
                $data_result[] = $row_result;
            }
        
        
        $output = array(
            "draw" => $_POST['draw'],
            "recordsTotal" => $this->datatable->count_all(),
            "recordsFiltered" => $this->datatable->count_filtered(),
            "data" => $data_result,
        );
        //output to json format
        echo json_encode($output);
    }
    
    function admin_order()
    {
        set_page('admin_order');
    }
    
    function get_order()
    {
		$admin_role = $this->input->post('admin_role');
		if(isset($_POST['admin_role'])){
			$join = $_POST['admin_role'];
        }
        //$where['order_status'] = 'open';
        //$data = $this->crud->get_admin_order($where,$join);
        
        $config['select'] = 'admin.admin_name, category.category_name, item.item_name, company.company_name, variant.variant_name, customer_order.customer_admin_id, customer_order.quantity, customer_order.order_status, customer_order.create_date';
        $config['table'] = 'customer_order';
        $config['wheres'] = array();
        $config['wheres'][] = array('column_name'=>'order_status','column_value'=> 'open' );
        if(isset($_POST['admin_role']) && $_POST['admin_role'] == 'customer'){
            $config['wheres'][] = array('column_name'=>'employee_admin_id','column_value'=> '0' );
        } else {
            $config['wheres'][] = array('column_name'=>'employee_admin_id !=','column_value'=> '0' );
        }
        $config['joins'][] = array('join_table'=>'admin','join_by'=>'customer_order.'.$join.'_admin_id = admin.admin_id','join_type'=>'left');
        $config['joins'][] = array('join_table'=>'product','join_by'=>'customer_order.product_id = product.product_id','join_type'=>'left');
        $config['joins'][] = array('join_table'=>'category','join_by'=>'product.category_id = category.category_id','join_type'=>'left');
        $config['joins'][] = array('join_table'=>'item','join_by'=>'product.item_id = item.item_id','join_type'=>'left');
        $config['joins'][] = array('join_table'=>'company','join_by'=>'product.company_id = company.company_id','join_type'=>'left');
        $config['joins'][] = array('join_table'=>'variant','join_by'=>'product.variant_id = variant.variant_id','join_type'=>'left');
        $config['column_search'] = array('admin.admin_name', 'category.category_name', 'item.item_name', 'company.company_name', 'variant.variant_name');
        $config['order'] = array('customer_order.create_date' => 'desc');
        
        
        $this->load->library('datatables', $config, 'datatable');
        $list = $this->datatable->get_datatables();
		//$data
		$data_result = array();
        
		foreach($list as $row)
		{
            if($admin_role == 'employee')
            {
                $where['admin_id'] = $row->customer_admin_id;
                $e_res = $this->crud->get_all_with_where('admin','','',$where);
                $cust_emp_name = "<b>Employee Name: </b> ".$row->admin_name."<br /><b>Customer Name: </b> ".$e_res[0]->admin_name."<br />";
            } else {
                $cust_emp_name = "<b>Customer Name: </b> ".$row->admin_name."<br />";
            }
			$row_result = array();
            $date = $row->create_date;
            $date = substr($date, 8, 2).'-'.substr($date, 5, 2).'-'.substr($date, 0, 4).' '.substr($date, 11);
            $row_result[] = $cust_emp_name."
                            <b>Product Name: </b> <br />".$row->category_name." " .$row->item_name." ".$row->company_name." ".$row->variant_name."<br />
                            <b>Quantity: </b> ".$row->quantity."<br />
                            <b>Status: </b> ".$row->order_status."<br />
                            <b>Date: </b> ".$date."<br /><br />";
			$data_result[] = $row_result;
		}
		
        $output = array(
            "draw" => $_POST['draw'],
            "recordsTotal" => $this->datatable->count_all(),
            "recordsFiltered" => $this->datatable->count_filtered(),
            "data" => $data_result,
        );
        //output to json format
        echo json_encode($output);
    }
}
?>
