<?php
use sngrl\PhpFirebaseCloudMessaging\Client;
use sngrl\PhpFirebaseCloudMessaging\Message;
use sngrl\PhpFirebaseCloudMessaging\Recipient\Device;
use sngrl\PhpFirebaseCloudMessaging\Notification;
class Sendnoti Extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
		$this->load->model('Crud', 'crud');
    }

	function index(){
		$server_key = $this->config->item('api_server_key');
		$client = new Client();
		$client->setApiKey($server_key);
		$client->injectGuzzleHttpClient(new \GuzzleHttp\Client());

		$token_value = $this->crud->get_token_value('','1');
		$token_value = $token_value[0]->token;

		$message = new Message();
		$message->setPriority('high');
		$message->addRecipient(new Device($token_value));
		$message
			->setNotification(new Notification('some title', 'some body testing'))
			->setData(['openLink' => 'http://omvir.com/aditya_enterprise/auth/register'])
		;
		$response = $client->send($message);
		var_dump($response->getStatusCode());
		var_dump($response->getBody()->getContents());
	}
}
?>
