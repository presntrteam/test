<?php
use sngrl\PhpFirebaseCloudMessaging\Client;
use sngrl\PhpFirebaseCloudMessaging\Message;
use sngrl\PhpFirebaseCloudMessaging\Recipient\Device;
use sngrl\PhpFirebaseCloudMessaging\Notification;
class Customer Extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
        if (null == ($this->session->userdata('is_logged_in'))) {
			$this->load->helper('url');
			$this->session->set_userdata('redirect_back', current_url());
            redirect('/auth/login/');
        }
        $this->load->model("Appmodel", "app_model");
        $this->load->model('Crud', 'crud');
        $this->load->helper('url');
        $this->load->library('email');
    }

    function index()
    {

    }


    function order_from_customer()
    {
        $data['category_data'] = $this->app_model->get_category();
        $data['item_data'] = $this->app_model->get_item();
        $data['company_data'] = $this->app_model->get_company();
        $data['variant_data'] = $this->app_model->get_variant();
        set_page('order',$data);
    }

    function payment_query()
    {
        set_page('payment_query');
    }

    function new_offers()
    {
        set_page('new_offers');
    }

    function employee_place_order()
    {
        $data['product_id'] = $this->input->post('product_id');
        $data['customer_admin_id'] = $this->input->post('customer_id');
        $data['quantity'] = $this->input->post('quantity');
        $data['remark'] = $this->input->post('remark');
        $data['create_date'] = date('Y-m-d H:i:s');
        $data['order_status'] = 'not_confirmed';
        $data['employee_admin_id'] = $this->session->userdata('admin_id');
        $result = $this->app_model->insert('customer_order',$data);
        if($result)
        {
            $this->session->set_flashdata('success',true);
            if($this->input->post('btn') == 'add_more')
            {
                $this->session->set_flashdata('message','Product Added To Cart Successfully. Please Select Another Product');
            } 
            else
            {
                $this->session->set_flashdata('message','Product Added To Cart Successfully. Please Check Your Cart.');
            }
            echo json_encode($this->input->post('button_type'));
        }
        exit;
    }

    function place_order()
    {
        if(isset($_REQUEST)){
            if($this->session->userdata('admin_role') == 3)
            {
                $where['category_id'] = $this->input->post('category_id');
                $where['item_id'] = $this->input->post('item_id');
                $where['company_id'] = $this->input->post('company_id');
                $where['variant_id'] = $this->input->post('variant_id');
                $product_id = $this->crud->get_all_with_where('product','product_id','ASC',$where); 
                $data['product_id'] = $product_id[0]->product_id;
            }
            if($this->session->userdata('admin_role') == 2)
            {
                $data['product_id'] = $this->input->post('product_id');   
            }
            $data['quantity'] = $this->input->post('quantity');
            $data['remark'] = $this->input->post('remark');
            $data['create_date'] = date('Y-m-d H:i:s');
            $data['order_status'] = 'not_confirmed';
            $data['customer_admin_id'] = $this->session->userdata('admin_id');
            $result = $this->app_model->insert('customer_order',$data);
            if($result)
            {
                $this->session->set_flashdata('success',true);
                if($this->input->post('button_type') == 'add_more')
                {
                    $this->session->set_flashdata('message','Product Added To Cart Successfully. Please Select Another Product');
                } 
                else
                {
                    $this->session->set_flashdata('message','Product Added To Cart Successfully. Please Check Your Cart.');
                }
                echo json_encode($this->input->post('button_type'));
            }    
        }
        
    }

    function cart()
    {
        $where['customer_admin_id'] = $this->session->userdata('admin_id');
        $where['order_status'] = 'not_confirmed';
        $data['results'] = $this->crud->get_cart_product($where);
        set_page('cart',$data);
    }

    function employee_cart()
    {
        $where['employee_admin_id'] = $this->session->userdata('admin_id');
        $where['order_status'] = 'not_confirmed';
        $data['results'] = $this->crud->get_cart_product($where);
        set_page('employee_cart',$data);
    }
    
    function order_confirmed()
    {
        $customer_order_ids = $_POST['customer_order_id'];
        
        $order_records = $this->crud->get_rows_by_id('customer_order' , 'customer_order_id',$customer_order_ids);
        //echo "<pre>";print_r($order_records);
        
        $headers  = 'MIME-Version: 1.0' . "\r\n";
        $headers .= 'Content-type: text/html; charset=UTF-8' . "\r\n";
        $message = '<html><body>';
        if($order_records[0]->employee_admin_id != 0){
            $employee_name = $this->crud->get_id_by_val('admin','admin_name','admin_id',$order_records[0]->employee_admin_id);
            $message .= '<h4>Employee Name : ' . $employee_name .' </h4>';
            $from = "Employee";
        }else{
            $from = "Customer";
        }
        $customer_name = $this->crud->get_id_by_val('admin','admin_name','admin_id',$order_records[0]->customer_admin_id);
        $message .= '<h4>Customer Name : ' . $customer_name .' </h4>';
        foreach($order_records as $record){
            $where['customer_order_id'] = $record->customer_order_id;
            $rec = $this->crud->get_row_by_id('customer_order',$where);
            //echo'<pre>';print_r($rec);
            $products = $this->crud->product_name($rec[0]->product_id);
            $product_name = $products->category_name.' '.$products->item_name.' '.$products->company_name.' '.$products->variant_name;
            $message .= '<p>Product Name : '. $product_name.' </p>';
            $message .= '<p>Quantity : '. $record->quantity.' </p>';
            $message .= '<p>Remark : '. $record->remark.' </p>';
            
        }
        $message .= '</body></html>';
        
        if($this->session->userdata('admin_role') == 3)
        {
            $where['customer_admin_id'] = $this->session->userdata('admin_id');    
        }
        if($this->session->userdata('admin_role') == 2)
        {
            $where['employee_admin_id'] = $this->session->userdata('admin_id');    
        }
        //$where['order_status'] = 'not_confirmed';
        $data['order_status'] = 'open';
        foreach($customer_order_ids as $id){
            $where['customer_order_id'] = $id;
            $result = $this->crud->update('customer_order',$data,$where);
        }
        
        
        //$result = $this->crud->update('customer_order',$data,$where);
        if($result)
        {
            $this->session->set_flashdata('success',true);
            $this->session->set_flashdata('message','Order placed Successfully.');
            $return['success'] = "true";
            print json_encode($return);
            $mail = mail($this->config->item('send_email_to'), 'Added New Order By'.$from, $message, $headers);
            exit;
        }
        else
        {
            $this->session->set_flashdata('success',false);
            $this->session->set_flashdata('message','! Error : Order not placed.');
            $return['success'] = "false";
            print json_encode($return);
            exit;
        }
    }
    
    function my_orders()
    {
        $where['admin.admin_id'] = $this->session->userdata('admin_id');
        $where['customer_order.order_status'] = 'open';
        $data['records'] = $this->crud->get_my_orders($where);
        set_page('my_orders',$data);
    }

    function planning()
	{
		if($this->input->post()){
			$post = $this->input->post();
			if(empty($post['product_ids'])){
				$return['error'] = "product_ids_empty";
				print json_encode($return);
				exit;
			}
			$panning_date = substr($post['panning_date'], 6, 4).'-'.substr($post['panning_date'], 3, 2).'-'.substr($post['panning_date'], 0, 2);
			$panning_date = $panning_date.' 00:00:00';
			$where_array = array(
								"employee_admin_id" => $post['employee_admin_id'],
								"customer_admin_id" => $post['customer_admin_id'],
								"planning_date" => $panning_date
							);
			$row = $this->crud->get_duplicate('planning',$where_array);
			if($row < '0'){
				$planning_id = $this->crud->insert('planning',
												array(
													'employee_admin_id' => $post['employee_admin_id'], 
													'customer_admin_id' => $post['customer_admin_id'], 
													'planning_date' => $panning_date, 
													'create_date' => date('Y-m-d H:i:s'), 
												));
                $planning_rec = $this->crud->get_row_by_id('planning',array('planning_id' => $planning_id));
                
                
                
                $employee_name = $this->crud->get_id_by_val('admin','admin_name','admin_id',$planning_rec[0]->employee_admin_id);
                $customer_name = $this->crud->get_id_by_val('admin','admin_name','admin_id',$planning_rec[0]->customer_admin_id);
                $planning_date = $planning_rec[0]->planning_date;
				
                $headers  = 'MIME-Version: 1.0' . "\r\n";
                $headers .= 'Content-type: text/html; charset=UTF-8' . "\r\n";
                $message = '<html><body>';
                $message .= '<p>Employee Name : '. $employee_name.' </p>';
                $message .= '<p>Customer Name : '. $customer_name.' </p>';
                $message .= '<p>Planing Date : '. $planning_date.' </p>';
                
                foreach($post['product_ids'] as $product_id){
					$planning_pro_id = $this->crud->insert('planning_product',
													array(
														'planning_id' => $planning_id, 
														'product_id' => $product_id, 
													));
                    $products = $this->crud->product_name($product_id);
                    $product_name = $products->category_name.' '.$products->item_name.' '.$products->company_name.' '.$products->variant_name;
                    $message .= '<p>Product Name : '. $product_name.' </p>';
            
				}
                $message .= '</body></html>';
                
				if($planning_pro_id){
                    $this->session->set_flashdata('success',true);
                    $this->session->set_flashdata('message','Planning Saved Successfully.');
                    $mail = mail($this->config->item('send_email_to'), 'New Planing Notification From '.$employee_name, $message, $headers);
					exit;
				}
			} else {
                $this->session->set_flashdata('success',false);
                $this->session->set_flashdata('message','Planning Already Exists.');
				exit;
			}
		}
		$data = array();
		$data['customers'] = $this->app_model->get_all_customers();
		$data['products'] = $this->crud->product_list();
		$data['plannings'] = $this->crud->planning_list();
		set_page('planning',$data);
	}

    function offers()
    {
		$data['offers'] = $this->crud->get_product_offer();
        set_page('offers', $data);
    }

    function employee_order()
    {
        $data = array(
            'products' => $this->crud->product_list(),
            'customers' => $this->app_model->get_all_customers(),
        );
        set_page('employee_order',$data);
    }

    function complains()
    {
		$data['products'] = $this->crud->product_list();
        $data['customers'] = $this->app_model->get_all_customers();
        set_page('problem_handling',$data);
    }
    
    function my_complains()
    {
        $admin = 'customer';
        $admin2 = 'employee';
        if($this->session->userdata('admin_role') == 2)
        {
            $admin = 'employee';
            $admin2 = 'customer';
        }
        $where['admin.admin_id'] = $this->session->userdata('admin_id');
        $data['records'] = $this->crud->complain_list($where,$admin,$admin2);
        set_page('my_complains',$data);
    }
    
    function reply_complain($complain_id)
    {
        $where['complain_id'] = $complain_id;
        $data['complain_id'] = $complain_id;
        $complain = $this->crud->get_complain_detail($where);
        $data['product_name'] = $complain[0]->category_name.' '.$complain[0]->item_name.' '.$complain[0]->company_name.' '.$complain[0]->variant_name;
        $data['create_date'] = $complain[0]->create_date;
        $data['product_image'] = $complain[0]->product_image;
        $data['admin_name'] = $complain[0]->admin_name;
        $data['issue'] = $complain[0]->remark;
        $data['results'] = $this->crud->get_complain_reply($where);
        set_page('reply_complain',$data);
    }

    function submit_complain_reply()
    {
        $data = $this->input->post();
        $data['create_date'] = date('Y-m-d H:i:s');
        $data['sender_id'] = $this->session->userdata('admin_id');
        $result = $this->crud->insert('complain_reply',$data);
        if($result)
        {
            $this->session->set_flashdata('success',true);
            $this->session->set_flashdata('message','Replied Successfully.');
            $return['success'] = "true";
            print json_encode($return);
            exit;
        }
        else
        {
            $this->session->set_flashdata('success',false);
            $this->session->set_flashdata('message','! Error : Not Replied.');
            $return['success'] = "false";
            print json_encode($return);
            exit;
        }
    }

    function knowledge_center()
    {
		$data['products'] = $this->crud->product_list();
        set_page('knowledge_center',$data);
    }
    
    function add_problem()
    {
		if($this->input->post()){
            $post_data = $this->input->post();
            $picture = '';
            $customer_admin_id = $post_data['customer_admin_id'];
            $product_id = $post_data['product_id'];
            
             if(!empty($_FILES['img']['name'])){
                $config['upload_path'] = 'assets/uploads/complain/';
                $config['allowed_types'] = 'jpg|jpeg|png|gif';
                $config['file_name'] = $_FILES['img']['name'];
                
                //Load upload library and initialize configuration
                $this->load->library('upload',$config);
                $this->upload->initialize($config);
                
                if($this->upload->do_upload('img')){
                    $uploadData = $this->upload->data();
                    $picture = $uploadData['file_name'];
                }else{
                    $picture = '';
                    $this->session->set_flashdata('success',false);
                    $this->session->set_flashdata('message','!  : Image File Not Uploded.');
                    exit;
                }
            }  
            
            $remarks = $post_data['remarks'];
            $admin_role = $post_data['admin_role'];
            $admin_id = $post_data['admin_id'];
            
            $token_value = $this->crud->get_token_value('','1');
            $token_value = $token_value[0]->token;
            
            $data = array();
            if($admin_role == '3'){
                $data['employee_admin_id'] = null;
                $data['customer_admin_id'] = $customer_admin_id;
            }else{
                $data['employee_admin_id'] = $admin_id;
                $data['customer_admin_id'] = $customer_admin_id;
            }
            $data['product_id'] = $product_id;
            $data['product_image'] = $picture;
            $data['remark'] = $remarks;
            $data['create_date'] = date("Y-m-d H:i:s");
            $data['status'] = 'open';
            
            
            $message = array();
            $products = $this->crud->product_name($product_id);
            $product_name = $products->category_name.' '.$products->item_name.' '.$products->company_name.' '.$products->variant_name;
            if($admin_role == '3'){
				$employee_admin_name = null;
                $customer_admin_name = $this->crud->get_id_by_val('admin','admin_name','admin_id',$data['customer_admin_id']);
                $from = 'Customer';
            }else{
                $employee_admin_name = $this->crud->get_id_by_val('admin','admin_name','admin_id',$data['employee_admin_id']);
				$customer_admin_name = $this->crud->get_id_by_val('admin','admin_name','admin_id',$data['customer_admin_id']);
				$from = 'Employee';
            }
            $remark = $remarks;
            
            $message = '<html><body>';
			if(!empty($employee_admin_name)){
				$message .= '<p>Employee Name : '. $employee_admin_name. '</p>';
			}
			$message .= '<p>Customer Name : '. $customer_admin_name.' </p>';
			$message .= '<p>Product Name : '. $product_name.' </p>';
			$message .= '<p>Remarks : ' . $remark .' </p>';
			$message .= '</body></html>';
			
			$headers  = 'MIME-Version: 1.0' . "\r\n";
			$headers .= 'Content-type: text/html; charset=UTF-8' . "\r\n";

            $result = $this->crud->insert("complain", $data);
            if($result)
            {
				
				$server_key = $this->config->item('api_server_key');
				$client = new Client();
				$client->setApiKey($server_key);
				$client->injectGuzzleHttpClient(new \GuzzleHttp\Client());

				$noti_message = new Message();
				$noti_message->setPriority('high');
				$noti_message->addRecipient(new Device($token_value));
				$noti_message
					->setNotification(new Notification('Complain Notofication', 'From '.$from))
					->setData(['openLink' => 'http://omvir.com/aditya_enterprise/admin/customer_complains'])
				;
				$client->send($noti_message);
				
                $this->session->set_flashdata('success',true);
                $this->session->set_flashdata('message','Problem Reported Successfully.');
                $return['success'] = "true";
                print json_encode($return);
                $mail = mail($this->config->item('send_email_to'), 'Complain Notofication From '.$from, $message, $headers);
                exit;
            }
            else
            {
                $this->session->set_flashdata('success',false);
                $this->session->set_flashdata('message','! Error : Problem Not Reported.');
                $return['success'] = "false";
                print json_encode($return);
                exit;
            }
        }
    }
    
    
    function get_item()
    {
        if($this->input->post())
        {
            $result = $this->crud->get_item($this->input->post('category_id'));
            echo "<option value=''> - Select Item - </option>";
            foreach ($result as $row) {
                $item_id = $row->item_id;
                $item_name = $row->item_name;
                echo '<option value="'.$item_id.'">'.$item_name.'</option>';
            }
        }
    }

    function get_company()
    {
        $category_id = $this->input->post('category_id');
        $item_id = $this->input->post('item_id');
        $result = $this->crud->get_company($category_id,$item_id);
        echo "<option value=''> - Select Company - </option>";
        foreach ($result as $row) {
            $company_id = $row->company_id;
            $company_name = $row->company_name;
            echo '<option value="'.$company_id.'">'.$company_name.'</option>';
        }
    }

    function get_variant()
    {
        $category_id = $this->input->post('category_id');
        $item_id = $this->input->post('item_id');
        $company_id = $this->input->post('company_id');
        $result = $this->crud->get_variant($category_id,$item_id,$company_id);
        echo "<option value=''> - Select Variant - </option>";
        foreach ($result as $row) {
            $variant_id = $row->variant_id;
            $variant_name = $row->variant_name;
            echo '<option value="'.$variant_id.'">'.$variant_name.'</option>';
        }
    }

    function delete($id)
    {
        $table = $_POST['table_name'];
        $id_name = $_POST['id_name'];
        $this->crud->delete($table, array($id_name => $id));
        $this->session->set_flashdata('success',true);
        $this->session->set_flashdata('message','Deleted Successfully');
    }
    
    function get_product_details()
    {
        $where['product_id'] = $this->input->post('product_id');
        $data = $this->crud->get_all_with_where('product','','',$where);
        $product_description = $data[0]->product_description;
        echo $product_description;
    }

}
?>
