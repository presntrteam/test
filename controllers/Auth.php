<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Auth extends CI_Controller
{

    function __construct()
    {
        parent::__construct();
        $this->load->model("Appmodel", "app_model");
        $this->load->model('Crud', 'crud');
        $this->load->library('form_validation');
        //$this->output->enable_profiler('false');
    }

    function index()
    {
		if(isset($_REQUEST['token'])){
			$this->session->set_userdata('current_token',$_REQUEST['token']);
		}
        if ($this->session->userdata('is_logged_in')) {
            $data = array();
            if($this->session->userdata('admin_role') == 2)
            {
                $where = array();
                $where_customer['planning_date'] = date('Y-m-d').' 00:00:00';
                $where_customer['employee_admin_id'] = $this->session->userdata('admin_id');
                $cust_to_visit = $this->crud->get_today_visit($where_customer);
                $data['cust_to_visit'] = $cust_to_visit->result();
                $data['records'] = $this->crud->admin_complain_list($where);
            }
            set_page('dashboard',$data);
        } else {
            redirect('/auth/login/');
        }
    }

    /**
     * Login user on the site
     *
     * @return void
     */
    function login()
    {
        if ($this->session->userdata('is_logged_in')) {                                    // logged in
            redirect('');
        } else {
            $this->form_validation->set_rules('email','email', 'trim|required|valid_email');
            $this->form_validation->set_rules('pass', 'password', 'trim|required');
            $this->form_validation->set_rules('remember', 'Remember me', 'integer');
            $data['errors'] = array();
            if ($this->form_validation->run()) {
                $email = $_POST['email'];
                $pass = $_POST['pass'];
                $response = $this->app_model->login($email,$pass);
                if ($response) {
                    $this->session->set_userdata('is_logged_in',$response[0]);
                    $this->session->set_userdata('admin_id',$response[0]['admin_id']);
                    $this->session->set_userdata('admin_name',$response[0]['admin_name']);
                    $this->session->set_userdata('admin_email',$response[0]['admin_email']);
                    $this->session->set_userdata('admin_role',$response[0]['admin_role']);
                    $this->session->set_flashdata('success',true);
                    //$this->session->set_flashdata('message','You have successfully login.');
                    $this->app_model->remove_token($this->session->userdata('current_token'));
                    $this->app_model->update_token($response[0]['admin_id'],$this->session->userdata('current_token'));
                    // user is authenticated, lets see if there is a redirect:
					if( $this->session->userdata('redirect_back') ) {
						$redirect_url = $this->session->userdata('redirect_back');  // grab value and put into a temp variable so we unset the session value
						$this->session->unset_userdata('redirect_back');
						redirect( $redirect_url );
					} else {
						redirect('');
					}
                } else {
                    $data['errors']['invalid'] = 'Invalid email or password!';
                }
            } else {
                if (validation_errors()) {
                    $error_messages = $this->form_validation->error_array();
                    $data['errors'] = $error_messages;
                }
            }
            $this->load->view('login_form', $data);
        }
    }
    
    function logout()
    {
		$current_token = $this->session->userdata('current_token');
        $this->session->unset_userdata('is_logged_in');
        session_destroy();
        $this->session->set_userdata('current_token', $current_token);
        redirect('auth/login/'.$current_token);
    }

    
    function profile()
    {
        if(!$this->session->userdata('is_logged_in')) {                                    // logged in
            redirect('');
        }
        $data = array();
        $query = $this->db->get_where('admin',array('admin_id',$this->session->userdata('is_logged_in')['admin_id']));

        set_page('change_password',$query->row());
        
    }
    
    function change_password()
    {
        $data = array();
        if (!empty($_POST)) {
            $this->form_validation->set_rules('admin_id', 'Admin ID', 'trim|required');
            $this->form_validation->set_rules('old_pass', 'old password', 'trim|required|callback_check_old_password');
            $this->form_validation->set_rules('new_pass', 'new password', 'trim|required');
            $this->form_validation->set_rules('confirm_pass', 'confirm Password', 'trim|required|matches[new_pass]');
            if ($this->form_validation->run()) {
                $this->db->where('admin_id',$_POST['admin_id']);
                $this->db->update('admin',array('admin_password'=>md5($_POST['new_pass'])));
                $this->session->set_flashdata('success',true);
                $this->session->set_flashdata('message','You have successfully changed password!');
                redirect('auth/profile');
            } else {
                if (validation_errors()) {
                    $error_messages = $this->form_validation->error_array();
                    $data['errors'] = $error_messages;
                }
            }
            set_page('change_password',$data);
        } else {
            redirect('auth/profile/');
        }
    }

    function check_old_password($old_pass){
        $admin_id = $_POST['admin_id'];
        $query = $this->db->get_where('admin',array('admin_id'=>$admin_id,'admin_password'=>md5($old_pass)));
        if($query->num_rows() > 0){
            return true;
        }else{
            $this->form_validation->set_message('check_old_password', 'wrong old password.');
            return false;
        }
    }
    
    function register(){
         $this->load->view('register');
    }

    function register_submit()
    {
        $data['admin_name'] = $this->input->post('admin_name');
        $data['organization'] = $this->input->post('organization');
        $data['address'] = $this->input->post('address');
        $data['admin_city'] = $this->input->post('admin_city');
        $data['admin_email'] = $this->input->post('admin_email');
        $dob2 = $dob = $this->input->post('dob');
        $data['dob'] = substr($dob, 6, 4).'-'.substr($dob, 3, 2).'-'.substr($dob, 0, 2);
        $data['admin_tin_no'] = $this->input->post('admin_tin_no');
        $data['admin_password'] = $this->input->post('admin_password');
        $data['admin_status'] = 'pending';
        $data['create_date'] = date('d-m-Y');
        //print_r($data); exit();
        $response = $this->app_model->checkEmailRegisterd($data['admin_email']);
        if ($response) {
			$data['errors']['exist'] = 'Email Id Already Exist!';
            $data['dob'] = $dob2;
			$this->load->view('register', $data);
		} else {
			$this->app_model->register($data);
			$this->load->view('requested',$data);
		}
    }
}
